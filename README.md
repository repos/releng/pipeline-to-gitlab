This is a script which can convert a simple pipelinelib config into a .gitlab-ci.yml file.

Example usage:
```
pipeline-to-gitlab .pipeline/config.yaml > .gitlab-ci.yml
```

Refs:
https://wikitech.wikimedia.org/wiki/PipelineLib
